package com.bertalandancs.icelltest.data

import com.bertalandancs.icelltest.data.model.ErrorResponse
import com.google.gson.Gson
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class HttpErrorInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalResponse = chain.proceed(chain.request())

        if (!originalResponse.isSuccessful && originalResponse.body != null) {
            val errorResponse: ErrorResponse =
                Gson().fromJson(originalResponse.body!!.string(), ErrorResponse::class.java)
            throw ServiceErrorException(errorResponse)
        } else
            return originalResponse
    }
}

data class ServiceErrorException(val errorResponse: ErrorResponse) : IOException()