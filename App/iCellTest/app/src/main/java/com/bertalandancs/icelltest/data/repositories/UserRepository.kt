package com.bertalandancs.icelltest.data.repositories

import com.bertalandancs.icelltest.data.api.GithubService
import com.bertalandancs.icelltest.data.model.DetailedUser
import com.bertalandancs.icelltest.data.model.Repo
import io.reactivex.rxjava3.core.Observable

interface UserRepository {

    fun getUserByLoginName(loginName: String): Observable<DetailedUser>

    fun getUserRepos(loginName: String, page: Int = 1): Observable<List<Repo>>

}

///TODO: We can use local storage or cache too
class UserRepositoryImpl(private val githubService: GithubService) : UserRepository {
    override fun getUserByLoginName(loginName: String): Observable<DetailedUser> =
        githubService.getUserByLoginName(loginName)


    override fun getUserRepos(loginName: String, page: Int): Observable<List<Repo>> =
        githubService.getReposByUser(loginName, page)

}