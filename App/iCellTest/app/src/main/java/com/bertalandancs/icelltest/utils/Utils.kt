package com.bertalandancs.icelltest.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import timber.log.Timber


class Utils {
    companion object {
        fun hideKeyboardFrom(context: Context, view: View) {
            val imm: InputMethodManager =
                context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

        fun openBrowser(ctx: Context, uri: Uri) {
            Timber.d("perform open browser for: $uri")
            val defaultBrowser =
                Intent.makeMainSelectorActivity(Intent.ACTION_MAIN, Intent.CATEGORY_APP_BROWSER)
            defaultBrowser.data = uri
            if (defaultBrowser.resolveActivity(ctx.packageManager) != null)
                ctx.startActivity(defaultBrowser)
            ///TODO: Handle when you don't have browser
        }

        fun setTitle(appCompatActivity: AppCompatActivity, titleStr: String) {
            appCompatActivity.supportActionBar?.title = titleStr
        }

        fun showToast(context: Context, strId: Int) {
            Toast.makeText(
                context,
                strId,
                Toast.LENGTH_LONG
            ).show()
        }

        fun showToast(context: Context, string: String) {
            Toast.makeText(
                context,
                string,
                Toast.LENGTH_LONG
            ).show()
        }
    }
}