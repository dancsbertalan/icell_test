package com.bertalandancs.icelltest.ui.user.details

import androidx.lifecycle.ViewModel
import com.bertalandancs.icelltest.data.ServiceErrorException
import com.bertalandancs.icelltest.data.model.DetailedUser
import com.bertalandancs.icelltest.data.repositories.UserRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.PublishSubject
import timber.log.Timber
import java.net.SocketTimeoutException
import java.net.UnknownHostException

open class DetailsStatus {
    object InternetError : DetailsStatus()
    object UnknownError : DetailsStatus()
    object UserNotExists : DetailsStatus()
    object ApiLimitReached : DetailsStatus()

    data class Ok(val detailedUser: DetailedUser) : DetailsStatus()
    data class Loading(val isLoading: Boolean) : DetailsStatus()
}

class UserDetailsViewModel(private val userRepository: UserRepository) : ViewModel() {

    private val userDetails = PublishSubject.create<DetailsStatus>()
    val userDetailsObservable: Observable<DetailsStatus> = userDetails
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    fun getUserDetails(loginName: String) {
        userRepository.getUserByLoginName(loginName)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe {
                userDetails.onNext(DetailsStatus.Loading(true))
            }
            .subscribe({
                userDetails.onNext(DetailsStatus.Loading(false))
                userDetails.onNext(DetailsStatus.Ok(it))
            }, {
                Timber.e("getUserDetails for $loginName error: $it")
                userDetails.onNext(DetailsStatus.Loading(false))
                when (it) {
                    is SocketTimeoutException,
                    is UnknownHostException -> userDetails.onNext(DetailsStatus.InternetError)
                    is ServiceErrorException -> handleServiceException(it)
                    else -> userDetails.onNext(DetailsStatus.UnknownError)
                }
            })
    }

    private fun handleServiceException(sexception: ServiceErrorException) {
        if (sexception.errorResponse.message.contains("API rate limit exceeded"))
            userDetails.onNext(DetailsStatus.ApiLimitReached)
        else if (sexception.errorResponse.message == "Not Found")
            userDetails.onNext(DetailsStatus.UserNotExists)
    }
}