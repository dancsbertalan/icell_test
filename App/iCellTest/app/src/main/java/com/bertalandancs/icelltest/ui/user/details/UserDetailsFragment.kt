package com.bertalandancs.icelltest.ui.user.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.bertalandancs.icelltest.R
import com.bertalandancs.icelltest.data.model.DetailedUser
import com.bertalandancs.icelltest.databinding.UserDetailsFragmentBinding
import com.bertalandancs.icelltest.utils.Utils
import com.bertalandancs.icelltest.utils.Utils.Companion.showToast
import com.squareup.picasso.Picasso
import io.reactivex.rxjava3.disposables.CompositeDisposable
import timber.log.Timber


class UserDetailsFragment(private val viewModel: UserDetailsViewModel) : Fragment() {

    private var _binding: UserDetailsFragmentBinding? = null
    private val binding get() = _binding!!
    private val disposable: CompositeDisposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = UserDetailsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSubscriptions()

        val loginName = arguments?.getString(LOGIN_NAME)
        if (!loginName.isNullOrEmpty()) {
            viewModel.getUserDetails(loginName)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        disposable.clear()
        viewModelStore.clear()
    }

    private fun initSubscriptions() {
        disposable.add(
            viewModel.userDetailsObservable.subscribe({
                Timber.d("userDetails next item: $it")
                when (it) {
                    is DetailsStatus.Loading -> {
                        binding.progressIndicator.isVisible = it.isLoading
                        binding.detailsContainer.isVisible = !it.isLoading
                    }
                    is DetailsStatus.InternetError -> showToast(
                        requireContext(),
                        R.string.msg_internet_error
                    )
                    is DetailsStatus.UnknownError -> showToast(
                        requireContext(),
                        R.string.msg_unknown_error
                    )
                    is DetailsStatus.UserNotExists -> showToast(
                        requireContext(),
                        R.string.msg_user_not_exists
                    )
                    is DetailsStatus.ApiLimitReached -> showToast(
                        requireContext(),
                        R.string.msg_api_limit_reached
                    )
                    is DetailsStatus.Ok -> setDetails(it.detailedUser)
                }
            },
                {
                    Timber.e("userDetails error: $it")
                    showToast(
                        requireContext(),
                        getString(R.string.msg_unknown_error_param, it)
                    )
                })
        )
    }

    private fun setDetails(detailedUser: DetailedUser) {
        Timber.d("setDetails for $detailedUser")
        val titleStr =
            if (!detailedUser.name.isNullOrEmpty()) detailedUser.name else detailedUser.login
        Utils.setTitle(requireActivity() as AppCompatActivity, titleStr!!)
        Picasso.get().load(detailedUser.avatarUrl).into(binding.avatar)

        binding.created.value = detailedUser.createdAt
        binding.updated.value = detailedUser.updatedAt
        binding.loginName.value = detailedUser.login

        detailedUser.bio?.let {
            binding.bio.text = it
            binding.bio.isVisible = true
        }
        detailedUser.company?.let {
            binding.company.value = it
            binding.company.isVisible = true
        }
        detailedUser.location?.let {
            binding.location.value = it
            binding.location.isVisible = true
        }
        detailedUser.email?.let {
            binding.email.value = it
            binding.email.isVisible = true
        }
        detailedUser.twitterUsername?.let {
            binding.twitter.value = it
            binding.twitter.isVisible = true
        }

        binding.avatar.setOnClickListener {
            Utils.openBrowser(requireContext(), detailedUser.htmlUrl.toUri())
        }

        binding.publicRepos.value = detailedUser.publicRepos.toString()
        binding.publicRepos.setOnClickListener { view ->
            detailedUser.publicRepos?.let {
                if (it > 0)
                    Navigation.findNavController(view)
                        .navigate(
                            R.id.action_userDetailsFragment_to_repoListFragment,
                            Bundle().apply {
                                putString(LOGIN_NAME, detailedUser.login)
                            })
            }
        }

        binding.progressIndicator.isVisible = false
        binding.detailsContainer.isVisible = true
    }


    companion object {
        private const val LOGIN_NAME = "LOGIN_NAME"
    }

}