package com.bertalandancs.icelltest.ui.user.repos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bertalandancs.icelltest.R
import com.bertalandancs.icelltest.databinding.RepoListFragmentBinding
import com.bertalandancs.icelltest.utils.Utils
import com.bertalandancs.icelltest.utils.Utils.Companion.showToast
import io.reactivex.rxjava3.disposables.CompositeDisposable
import timber.log.Timber

class RepoListFragment(private val viewModel: RepoListViewModel) : Fragment() {

    private var _binding: RepoListFragmentBinding? = null
    private val binding get() = _binding!!
    private val disposable: CompositeDisposable = CompositeDisposable()
    private val userReposResultAdapter: UserRepoResultItemAdapter = UserRepoResultItemAdapter()
    private lateinit var loginName: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = RepoListFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initSubscriptions()

        loginName = arguments?.getString(LOGIN_NAME).toString()
        if (loginName.isNotEmpty()) {
            viewModel.getRepos(loginName)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        disposable.clear()
        viewModelStore.clear()
    }

    private fun initSubscriptions() {
        disposable.add(viewModel.reposObservable.subscribe({
            Timber.d("Fetch user repos onNext: $it")
            when (it) {
                is ReposStatus.Loading -> {
                    binding.progressIndicator.isVisible = it.isLoading
                }
                is ReposStatus.Ok -> {
                    if (it.page > 1)
                        userReposResultAdapter.addToResultList(it.repoList)
                    else
                        userReposResultAdapter.results = it.repoList
                }
                is ReposStatus.InternetError -> showToast(
                    requireContext(),
                    R.string.msg_internet_error
                )
                is ReposStatus.UnknownError -> showToast(
                    requireContext(),
                    R.string.msg_unknown_error
                )
                is ReposStatus.MissingUserOrRepoByUser -> showToast(
                    requireContext(),
                    R.string.msg_missing_repos_for_user
                )
                is ReposStatus.ApiLimitReached -> showToast(
                    requireContext(),
                    R.string.msg_api_limit_reached
                )
            }
        }, {
            Timber.e("Fetch user repos onNext: $it")
            showToast(
                requireContext(),
                getString(R.string.msg_unknown_error_param, it)
            )
        }))
    }


    private fun initView() {
        val loginName = arguments?.getString(LOGIN_NAME)

        if (!loginName.isNullOrEmpty())
            Utils.setTitle(requireActivity() as AppCompatActivity, "$loginName's repos")

        with(binding.repoResults) {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@RepoListFragment.context).apply {
                adapter = userReposResultAdapter
            }
            this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if (!binding.progressIndicator.isVisible &&
                        !recyclerView.canScrollVertically(1)
                    )
                        if (!loginName.isNullOrEmpty())
                            viewModel.getRepos(loginName)
                }
            })
        }
    }

    companion object {
        private const val LOGIN_NAME = "LOGIN_NAME"
    }

}