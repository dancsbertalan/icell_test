package com.bertalandancs.icelltest.data.api

import com.bertalandancs.icelltest.data.model.*
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GithubService {

    @GET("/search/users")
    fun getUsers(
        @Query("q") q: String,
        @Query("page") page: Int = 1
    ): Observable<SearchUserResponse>

    @GET("/users/{loginName}")
    fun getUserByLoginName(
        @Path("loginName") loginName: String
    ): Observable<DetailedUser>

    @GET("/users/{loginName}/repos")
    fun getReposByUser(
        @Path("loginName") loginName: String,
        @Query("page") page: Int = 1
    ): Observable<List<Repo>>

}