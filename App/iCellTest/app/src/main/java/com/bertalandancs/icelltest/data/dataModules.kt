package com.bertalandancs.icelltest.data

import com.bertalandancs.icelltest.data.api.GithubService
import com.bertalandancs.icelltest.data.repositories.SearchRepository
import com.bertalandancs.icelltest.data.repositories.SearchRepositoryImpl
import com.bertalandancs.icelltest.data.repositories.UserRepository
import com.bertalandancs.icelltest.data.repositories.UserRepositoryImpl
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.logging.HttpLoggingInterceptor

private const val baseUrl: String = "https://api.github.com"

val networkModule = module {

    val logging = HttpLoggingInterceptor()
    logging.setLevel(HttpLoggingInterceptor.Level.BODY)

    fun provideClient() = OkHttpClient.Builder()
        .addInterceptor(logging)
        .addInterceptor(HttpErrorInterceptor())
        .build()

    fun provideRetrofit(client: OkHttpClient) = Retrofit.Builder()
        .client(client)
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .build()

    single { provideClient() }
    single { provideRetrofit(get()) }
}

val githubServiceModule = module {
    fun provideGithubService(retrofit: Retrofit): GithubService {
        return retrofit.create(GithubService::class.java)
    }

    single { provideGithubService(get()) }
}

val searchRepositoryModule = module {
    fun provideSearchRepository(githubService: GithubService): SearchRepository =
        SearchRepositoryImpl(githubService)

    single {
        provideSearchRepository(get())
    }
}

val userRepositoryModule = module {
    fun provideUserRepository(githubService: GithubService): UserRepository =
        UserRepositoryImpl(githubService)

    single {
        provideUserRepository(get())
    }
}
