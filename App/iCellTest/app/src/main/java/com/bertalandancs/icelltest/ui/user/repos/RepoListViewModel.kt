package com.bertalandancs.icelltest.ui.user.repos

import androidx.lifecycle.ViewModel
import com.bertalandancs.icelltest.data.ServiceErrorException
import com.bertalandancs.icelltest.data.model.Repo
import com.bertalandancs.icelltest.data.repositories.UserRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.PublishSubject
import timber.log.Timber
import java.net.SocketTimeoutException
import java.net.UnknownHostException

open class ReposStatus {
    data class Ok(val repoList: List<Repo>, val page: Int) : ReposStatus()
    data class Loading(val isLoading: Boolean) : ReposStatus()

    object InternetError : ReposStatus()
    object UnknownError : ReposStatus()
    object ApiLimitReached : ReposStatus()
    object MissingUserOrRepoByUser : ReposStatus()
}

class RepoListViewModel(private val userRepository: UserRepository) : ViewModel() {

    private var currentPage: Int = 1
    private val disposables = CompositeDisposable()
    private val repos = PublishSubject.create<ReposStatus>()
    val reposObservable: Observable<ReposStatus> = repos
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    fun getRepos(loginName: String) {
        disposables.add(
            userRepository.getUserRepos(loginName, currentPage)
                .doOnSubscribe {
                    repos.onNext(ReposStatus.Loading(true))
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    Timber.d("repos by $loginName page: $currentPage - repoCount: ${it.size} $it")
                    repos.onNext(ReposStatus.Loading(false))
                    repos.onNext(ReposStatus.Ok(it, currentPage))
                    currentPage += 1
                }, {
                    Timber.e("getUsers by $loginName - error: $it")
                    repos.onNext(ReposStatus.Loading(false))
                    when (it) {
                        is UnknownHostException,
                        is SocketTimeoutException -> repos.onNext(ReposStatus.InternetError)
                        is ServiceErrorException -> handleServiceErrorException(it)
                        else -> repos.onNext(ReposStatus.UnknownError)
                    }
                })
        )
    }

    private fun handleServiceErrorException(sexception: ServiceErrorException) {
        if (sexception.errorResponse.message.contains("API rate limit exceeded")) {
            repos.onNext(ReposStatus.ApiLimitReached)
        } else if (sexception.errorResponse.message.lowercase() == "not found"
            && sexception.errorResponse.documentationUrl.contains("#list-repositories-for-a-user")
        ) {
            repos.onNext(ReposStatus.MissingUserOrRepoByUser)
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}