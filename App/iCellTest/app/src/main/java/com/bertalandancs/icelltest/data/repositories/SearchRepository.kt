package com.bertalandancs.icelltest.data.repositories

import com.bertalandancs.icelltest.data.api.GithubService
import com.bertalandancs.icelltest.data.model.User
import io.reactivex.rxjava3.core.Observable

interface SearchRepository {

    fun getUsers(query: String, page: Int = 1): Observable<List<User>>

}

///TODO: We can use local storage or cache too
class SearchRepositoryImpl(private val githubService: GithubService) : SearchRepository {

    override fun getUsers(query: String, page: Int): Observable<List<User>> =
        githubService.getUsers(query, page).flatMap {
            Observable.just(it.items)
        }

}