package com.bertalandancs.icelltest

import android.app.Application
import com.bertalandancs.icelltest.data.githubServiceModule
import com.bertalandancs.icelltest.data.networkModule
import com.bertalandancs.icelltest.data.searchRepositoryModule
import com.bertalandancs.icelltest.data.userRepositoryModule
import com.bertalandancs.icelltest.ui.uiModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.fragment.koin.fragmentFactory
import org.koin.core.context.startKoin
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        //TODO: Otherwise logging to crash reporting sys

        startKoin {
            androidLogger()
            androidContext(this@App)
            fragmentFactory()
            koin.loadModules(
                listOf(
                    uiModule,
                    githubServiceModule,
                    networkModule,
                    userRepositoryModule,
                    searchRepositoryModule
                )
            )
        }
    }

}