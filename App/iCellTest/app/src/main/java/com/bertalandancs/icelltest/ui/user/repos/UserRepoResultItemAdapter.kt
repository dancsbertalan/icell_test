package com.bertalandancs.icelltest.ui.user.repos

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bertalandancs.icelltest.data.model.Repo
import com.bertalandancs.icelltest.databinding.UserRepoResultItemBinding
import com.bertalandancs.icelltest.utils.Utils
import timber.log.Timber

class UserRepoResultItemAdapter : RecyclerView.Adapter<UserRepoResultItemAdapter.ViewHolder>() {

    private lateinit var binding: UserRepoResultItemBinding
    private lateinit var ctx: Context

    var results: List<Repo> = ArrayList()
        set(value) {
            field = value
            notifyItemRangeInserted(0, results.size)
        }

    fun addToResultList(results: List<Repo>) {
        val oldSize = this.results.size
        (this.results as ArrayList<Repo>).addAll(results)
        notifyItemRangeInserted(oldSize, this.results.size - 1)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): UserRepoResultItemAdapter.ViewHolder {
        binding =
            UserRepoResultItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        ctx = parent.context
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: UserRepoResultItemAdapter.ViewHolder, position: Int) =
        holder.onBind(position)

    override fun getItemCount() = results.size

    inner class ViewHolder(private val binding: UserRepoResultItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind(position: Int) {
            val item = results[position]
            Timber.d("UserRepoResultItem - onBind with: $item")
            binding.repoName.text = item.name
            binding.updated.text = item.updatedAt
            binding.forkIcon.isVisible = item.fork
            binding.lockIcon.isVisible = item.private

            binding.itemContainer.setOnClickListener {
                Utils.openBrowser(ctx, item.htmlUrl.toUri())
            }
        }

    }
}