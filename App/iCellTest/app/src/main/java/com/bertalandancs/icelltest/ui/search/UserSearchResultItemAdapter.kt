package com.bertalandancs.icelltest.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bertalandancs.icelltest.R
import com.bertalandancs.icelltest.data.model.User
import com.bertalandancs.icelltest.databinding.UserSearchResultItemBinding
import com.squareup.picasso.Picasso
import timber.log.Timber

class UserSearchResultItemAdapter : RecyclerView.Adapter<UserSearchResultItemAdapter.ViewHolder>() {

    private lateinit var binding: UserSearchResultItemBinding

    var results: List<User> = ArrayList()
        set(value) {
            field = value
            notifyItemRangeInserted(0, results.size)
        }

    fun addToResultList(results: List<User>) {
        val oldSize = this.results.size
        (this.results as ArrayList<User>).addAll(results)
        notifyItemRangeInserted(oldSize, this.results.size - 1)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): UserSearchResultItemAdapter.ViewHolder {
        binding =
            UserSearchResultItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: UserSearchResultItemAdapter.ViewHolder, position: Int) =
        holder.onBind(position)

    override fun getItemCount() = results.size

    inner class ViewHolder(private val binding: UserSearchResultItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind(position: Int) {
            val item = results[position]
            Timber.d("UserSearchResultItem - onBind with: $item")

            Picasso.get().load(item.avatarUrl).into(binding.avatar)

            binding.avatar.setOnClickListener { navigateToUser(item) }
            binding.loginName.text = item.login
            binding.loginName.setOnClickListener { navigateToUser(item) }
            binding.type.text = item.type
            binding.type.setOnClickListener { navigateToUser(item) }
            binding.repos.setOnClickListener {
                Navigation.findNavController(itemView)
                    .navigate(R.id.action_searchFragment_to_repoListFragment, Bundle().apply {
                        putString(LOGIN_NAME, item.login)
                    })
            }
        }

        private fun navigateToUser(item: User) =
            Navigation.findNavController(itemView)
                .navigate(
                    R.id.action_searchFragment_to_userDetailsFragment,
                    Bundle().apply {
                        putString(LOGIN_NAME, item.login)
                    }
                )
    }

    companion object {
        private const val LOGIN_NAME = "LOGIN_NAME"
    }
}