package com.bertalandancs.icelltest.ui.search

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bertalandancs.icelltest.R
import com.bertalandancs.icelltest.databinding.SearchFragmentBinding
import com.bertalandancs.icelltest.utils.Utils
import com.bertalandancs.icelltest.utils.Utils.Companion.showToast
import io.reactivex.rxjava3.disposables.CompositeDisposable
import timber.log.Timber

class SearchFragment(private val viewModel: SearchViewModel) : Fragment() {

    private lateinit var searchView: SearchView

    private var _binding: SearchFragmentBinding? = null
    private val binding get() = _binding!!
    private val disposable: CompositeDisposable = CompositeDisposable()
    private val searchResultsAdapter = UserSearchResultItemAdapter()
    private var currentQuery: String = "first"
    private var firstLaunch = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = SearchFragmentBinding.inflate(inflater, container, false)
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)

        val searchItem = menu.findItem(R.id.search_item)
        searchView = searchItem?.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String): Boolean {
                Timber.d("onQueryTextSubmit: $query")
                if (query.length in 1..256) {
                    Utils.hideKeyboardFrom(requireContext(), searchView)
                    searchResultsAdapter.results = ArrayList()
                    currentQuery = query
                    viewModel.getUsers(query)
                } else
                    showToast(requireContext(), R.string.msg_query_length_between)
                return true
            }

            override fun onQueryTextChange(newText: String) = true
        })

        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initSubscriptions()

        if (firstLaunch) {
            viewModel.getUsers(currentQuery)
            firstLaunch = false
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        disposable.clear()
        viewModelStore.clear()
    }

    private fun initSubscriptions() {
        disposable.add(viewModel.usersSearchObservable.subscribe(
            {
                Timber.d("Fetch users onNext: $it")
                when (it) {
                    is SearchStatus.Loading -> {
                        binding.progressIndicator.isVisible = it.isLoading
                    }
                    is SearchStatus.Ok -> {
                        if (it.page > 1)
                            searchResultsAdapter.addToResultList(it.userList)
                        else
                            searchResultsAdapter.results = it.userList
                    }
                    is SearchStatus.InternetError -> showToast(
                        requireContext(),
                        R.string.msg_internet_error
                    )
                    is SearchStatus.ApiLimitReached -> showToast(
                        requireContext(),
                        R.string.msg_api_limit_reached
                    )
                    is SearchStatus.QueryInvalid -> showToast(
                        requireContext(),
                        R.string.msg_query_is_invalid
                    )
                    is SearchStatus.MissingQuery -> showToast(
                        requireContext(),
                        R.string.msg_query_param_missing
                    )
                    is SearchStatus.UnknownError -> showToast(
                        requireContext(),
                        R.string.msg_unknown_error
                    )
                }
            }, {
                Timber.e("Fetch users onError: $it")
                showToast(
                    requireContext(),
                    getString(R.string.msg_unknown_error_param, it)
                )
            })
        )
    }

    private fun initView() {
        with(binding.searchResults) {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@SearchFragment.context).apply {
                adapter = searchResultsAdapter
            }
            this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if (!binding.progressIndicator.isVisible &&
                        !recyclerView.canScrollVertically(1)
                    )
                        viewModel.getUsers(currentQuery)
                }
            })
        }
    }

}