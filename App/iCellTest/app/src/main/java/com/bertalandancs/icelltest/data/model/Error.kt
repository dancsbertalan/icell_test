package com.bertalandancs.icelltest.data.model

import com.google.gson.annotations.SerializedName

data class ErrorResponse(
    @SerializedName("message") var message: String,
    @SerializedName("documentation_url") var documentationUrl: String,
    @SerializedName("errors") var errors: List<Error> = emptyList()
)

data class Error(
    @SerializedName("message") var message: String = "",
    @SerializedName("resource") var resource: String,
    @SerializedName("field") var field: String,
    @SerializedName("code") var code: String
)