package com.bertalandancs.icelltest.data.model

import com.google.gson.annotations.SerializedName

data class SearchUserResponse(
    @SerializedName("total_count") var totalCount: Int,
    @SerializedName("incomplete_results") var incompleteResults: Boolean,
    @SerializedName("items") var items: List<User>,
)