package com.bertalandancs.icelltest.ui.search

import androidx.lifecycle.ViewModel
import com.bertalandancs.icelltest.data.ServiceErrorException
import com.bertalandancs.icelltest.data.model.User
import com.bertalandancs.icelltest.data.repositories.SearchRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.PublishSubject
import timber.log.Timber
import java.net.SocketTimeoutException
import java.net.UnknownHostException

open class SearchStatus {
    object InternetError : SearchStatus()
    object UnknownError : SearchStatus()
    object ApiLimitReached : SearchStatus()

    object QueryInvalid : SearchStatus()
    object MissingQuery : SearchStatus()

    data class Ok(val userList: List<User>, val page: Int) : SearchStatus()
    data class Loading(val isLoading: Boolean) : SearchStatus()
}

class SearchViewModel(private val searchRepository: SearchRepository) : ViewModel() {

    private val disposable: CompositeDisposable = CompositeDisposable()
    private val usersSearch = PublishSubject.create<SearchStatus>()
    private var currentPage = 0
    private var currentQuery = ""
    val usersSearchObservable: Observable<SearchStatus> = usersSearch
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())


    fun getUsers(query: String) {
        if (currentQuery != query)
            currentPage = 1

        disposable.add(
            searchRepository.getUsers(query, currentPage)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe {
                    usersSearch.onNext(SearchStatus.Loading(true))
                    currentQuery = query
                }
                .subscribe(
                    {
                        Timber.d("users by $query page: $currentPage - $it")
                        usersSearch.onNext(SearchStatus.Loading(false))
                        usersSearch.onNext(SearchStatus.Ok(it, currentPage))

                        if (currentQuery == query)
                            currentPage += 1
                    },
                    {
                        Timber.e("getUsers by $query - error: $it")
                        usersSearch.onNext(SearchStatus.Loading(false))
                        when (it) {
                            is UnknownHostException,
                            is SocketTimeoutException -> usersSearch.onNext(SearchStatus.InternetError)
                            is ServiceErrorException -> handleServiceErrorException(it)
                            else -> usersSearch.onNext(SearchStatus.UnknownError)
                        }
                    }
                ))
    }

    private fun handleServiceErrorException(serviceErrException: ServiceErrorException) {
        if (serviceErrException.errorResponse.message.contains("API rate limit exceeded")) {
            usersSearch.onNext(SearchStatus.ApiLimitReached)
        } else if (serviceErrException.errorResponse.errors.isNotEmpty()) {
            val errors = serviceErrException.errorResponse.errors
            for (error in errors) {
                if (error.code.lowercase() == "invalid" &&
                    error.field.lowercase() == "q" &&
                    error.resource.lowercase() == "search"
                )
                    usersSearch.onNext(SearchStatus.QueryInvalid)
                else if (error.code.lowercase() == "missing" &&
                    error.field.lowercase() == "q" &&
                    error.resource.lowercase() == "search"
                )
                    usersSearch.onNext(SearchStatus.MissingQuery)
            }
        }
        ///TODO: Handle when we got an "unhandled" error
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

}