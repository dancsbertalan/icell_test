package com.bertalandancs.icelltest.ui

import com.bertalandancs.icelltest.ui.search.SearchFragment
import com.bertalandancs.icelltest.ui.search.SearchViewModel
import com.bertalandancs.icelltest.ui.user.details.UserDetailsFragment
import com.bertalandancs.icelltest.ui.user.details.UserDetailsViewModel
import com.bertalandancs.icelltest.ui.user.repos.RepoListFragment
import com.bertalandancs.icelltest.ui.user.repos.RepoListViewModel
import org.koin.androidx.fragment.dsl.fragment
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val uiModule = module {

    fragment {
        SearchFragment(get())
    }
    viewModel {
        SearchViewModel(get())
    }

    fragment {
        UserDetailsFragment(get())
    }
    viewModel {
        UserDetailsViewModel(get())
    }

    fragment {
        RepoListFragment(get())
    }
    viewModel {
        RepoListViewModel(get())
    }

}