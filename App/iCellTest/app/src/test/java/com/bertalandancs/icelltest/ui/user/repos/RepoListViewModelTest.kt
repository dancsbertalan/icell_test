package com.bertalandancs.icelltest.ui.user.repos

import com.bertalandancs.icelltest.ViewModelBaseTestClass
import com.bertalandancs.icelltest.data.ServiceErrorException
import com.bertalandancs.icelltest.data.model.ErrorResponse
import com.bertalandancs.icelltest.data.model.Repo
import com.bertalandancs.icelltest.data.repositories.UserRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.kotlin.argThat
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class RepoListViewModelTest : ViewModelBaseTestClass() {

    private lateinit var sut: RepoListViewModel
    private lateinit var testObserver: TestObserver<ReposStatus>

    @Mock
    lateinit var userRepository: UserRepository

    @Before
    fun beforeEach() {
        sut = RepoListViewModel(userRepository)
        testObserver = TestObserver()
        sut.reposObservable.subscribeWith(testObserver)
    }

    @Test
    fun `when user has repos then post Ok status with repos and page number`() {
        val reposList = ArrayList<Repo>().apply {
            add(Repo("htmlUrl", 1, "Repo1", "2021-10-10", true, fork = true))
            add(Repo("htmlUrl", 2, "Repo2", "2021-10-10", private = false, fork = false))
            add(Repo("htmlUrl", 3, "Repo3", "2021-10-10", true, fork = false))
            add(Repo("htmlUrl", 4, "Repo4", "2021-10-10", private = false, fork = true))
        }
        `when`(userRepository.getUserRepos(argThat { this == "octo" }, anyInt()))
            .thenReturn(Observable.just(reposList))

        sut.getRepos("octo")

        testObserver.hasSubscription()
        testObserver.assertValues(
            ReposStatus.Loading(true),
            ReposStatus.Loading(false),
            ReposStatus.Ok(reposList, 1)
        )
    }

    @Test
    fun `when user has repos with more pages then post Ok status twice with repos and page number`() {
        val reposList = ArrayList<Repo>().apply {
            add(Repo("htmlUrl", 1, "Repo1", "2021-10-10", true, fork = true))
            add(Repo("htmlUrl", 2, "Repo2", "2021-10-10", private = false, fork = false))
            add(Repo("htmlUrl", 3, "Repo3", "2021-10-10", true, fork = false))
            add(Repo("htmlUrl", 4, "Repo4", "2021-10-10", private = false, fork = true))
        }
        val reposList2 = ArrayList<Repo>().apply {
            add(Repo("htmlUrl", 5, "Repo5", "2021-10-10", true, fork = true))
            add(Repo("htmlUrl", 6, "Repo6", "2021-10-10", private = false, fork = false))
            add(Repo("htmlUrl", 7, "Repo7", "2021-10-10", true, fork = false))
            add(Repo("htmlUrl", 8, "Repo8", "2021-10-10", private = false, fork = true))
        }
        `when`(userRepository.getUserRepos("octo", 1))
            .thenReturn(
                Observable.just(reposList)
            )
        `when`(userRepository.getUserRepos("octo", 2))
            .thenReturn(
                Observable.just(reposList2)
            )

        sut.getRepos("octo")
        sut.getRepos("octo")

        testObserver.hasSubscription()
        testObserver.assertValues(
            ReposStatus.Loading(true),
            ReposStatus.Loading(false),
            ReposStatus.Ok(reposList, 1),
            ReposStatus.Loading(true),
            ReposStatus.Loading(false),
            ReposStatus.Ok(reposList2, 2)
        )
    }

    @Test
    fun `when download user repos but got ServiceErrorException api limit reached then post ApiLimitReached status`() {
        `when`(userRepository.getUserRepos(anyString(), anyInt()))
            .thenReturn(
                Observable.error(
                    ServiceErrorException(
                        ErrorResponse("API rate limit exceeded", "docurl")
                    )
                )
            )

        sut.getRepos("noUser")

        testObserver.hasSubscription()
        testObserver.assertValues(
            ReposStatus.Loading(true),
            ReposStatus.Loading(false),
            ReposStatus.ApiLimitReached
        )
    }

    @Test
    fun `when download user repos but got ServiceErrorException with not found and user repos documentation then post MissingUserOrRepoByUser status`() {
        `when`(userRepository.getUserRepos(anyString(), anyInt()))
            .thenReturn(
                Observable.error(
                    ServiceErrorException(
                        ErrorResponse("Not Found", "*********#list-repositories-for-a-user")
                    )
                )
            )

        sut.getRepos("noUser")

        testObserver.hasSubscription()
        testObserver.assertValues(
            ReposStatus.Loading(true),
            ReposStatus.Loading(false),
            ReposStatus.MissingUserOrRepoByUser
        )
    }

    @Test
    fun `when download user repos but got unknown exception then post UnknownError status`() {
        `when`(userRepository.getUserRepos(anyString(), anyInt()))
            .thenReturn(
                Observable.error(
                    KotlinNullPointerException()
                )
            )

        sut.getRepos("unknownErrUser")

        testObserver.hasSubscription()
        testObserver.assertValues(
            ReposStatus.Loading(true),
            ReposStatus.Loading(false),
            ReposStatus.UnknownError
        )
    }

    @Test
    fun `when download user repos but got SocketTimeoutException then post InternetError status`() {
        `when`(userRepository.getUserRepos(anyString(), anyInt()))
            .thenReturn(
                Observable.error(
                    SocketTimeoutException()
                )
            )

        sut.getRepos("noNetUser")

        testObserver.hasSubscription()
        testObserver.assertValues(
            ReposStatus.Loading(true),
            ReposStatus.Loading(false),
            ReposStatus.InternetError
        )
    }

    @Test
    fun `when download user repos but got UnknownHostException then post InternetError status`() {
        `when`(userRepository.getUserRepos(anyString(), anyInt()))
            .thenReturn(
                Observable.error(
                    UnknownHostException()
                )
            )

        sut.getRepos("noNetUser")

        testObserver.hasSubscription()
        testObserver.assertValues(
            ReposStatus.Loading(true),
            ReposStatus.Loading(false),
            ReposStatus.InternetError
        )
    }

}