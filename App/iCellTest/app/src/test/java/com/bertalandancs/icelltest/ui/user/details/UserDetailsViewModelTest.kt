package com.bertalandancs.icelltest.ui.user.details

import com.bertalandancs.icelltest.ViewModelBaseTestClass
import com.bertalandancs.icelltest.data.ServiceErrorException
import com.bertalandancs.icelltest.data.model.DetailedUser
import com.bertalandancs.icelltest.data.model.ErrorResponse
import com.bertalandancs.icelltest.data.repositories.UserRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.Mockito.`when`
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class UserDetailsViewModelTest : ViewModelBaseTestClass() {

    private lateinit var sut: UserDetailsViewModel
    private lateinit var testObserver: TestObserver<DetailsStatus>

    @Mock
    lateinit var userRepository: UserRepository

    @Before
    fun beforeEach() {
        sut = UserDetailsViewModel(userRepository)
        testObserver = TestObserver()
        sut.userDetailsObservable.subscribeWith(testObserver)
    }

    @Test
    fun `when downloading user informations but user is not exists then post UserNotExists status`() {
        `when`(userRepository.getUserByLoginName(anyString())).thenReturn(
            Observable.error(
                ServiceErrorException(ErrorResponse("Not Found", "docUrl"))
            )
        )

        sut.getUserDetails("tUser")

        testObserver.hasSubscription()
        testObserver.assertValues(
            DetailsStatus.Loading(true),
            DetailsStatus.Loading(false),
            DetailsStatus.UserNotExists
        )
    }

    @Test
    fun `when downloading user informations but got ServiceErrorException with api limit reached then post ApiLimitReached status`() {
        `when`(userRepository.getUserByLoginName(anyString()))
            .thenReturn(
                Observable.error(
                    ServiceErrorException(
                        ErrorResponse("API rate limit exceeded", "docurl")
                    )
                )
            )

        sut.getUserDetails("apiUser")

        testObserver.hasSubscription()
        testObserver.assertValues(
            DetailsStatus.Loading(true),
            DetailsStatus.Loading(false),
            DetailsStatus.ApiLimitReached
        )
    }

    @Test
    fun `when downloading user informations and is success then post Ok status with DetailedUser`() {
        val expectedUser = DetailedUser(
            "tUser", 1, "",
            "Teszt User", "2021-10-10", "2021-10-09", ""
        )
        `when`(userRepository.getUserByLoginName("tUser")).thenReturn(
            Observable.just(
                expectedUser
            )
        )

        sut.getUserDetails("tUser")

        testObserver.hasSubscription()
        testObserver.assertValues(
            DetailsStatus.Loading(true),
            DetailsStatus.Loading(false),
            DetailsStatus.Ok(expectedUser)
        )
    }

    @Test
    fun `when downloading user informations but got unknown exception then post UnknownError status`() {
        `when`(userRepository.getUserByLoginName(anyString())).thenReturn(
            Observable.error(
                IllegalAccessException()
            )
        )

        sut.getUserDetails("illegalUser")

        testObserver.hasSubscription()
        testObserver.assertValues(
            DetailsStatus.Loading(true),
            DetailsStatus.Loading(false),
            DetailsStatus.UnknownError
        )
    }

    @Test
    fun `when downloading user informations but got SocketTimeoutException then post InternetError status`() {
        `when`(userRepository.getUserByLoginName(anyString())).thenReturn(
            Observable.error(
                SocketTimeoutException()
            )
        )

        sut.getUserDetails("socketUser")

        testObserver.hasSubscription()
        testObserver.assertValues(
            DetailsStatus.Loading(true),
            DetailsStatus.Loading(false),
            DetailsStatus.InternetError
        )
    }

    @Test
    fun `when downloading user informations but got UnknownHostException then post InternetError status`() {
        `when`(userRepository.getUserByLoginName(anyString())).thenReturn(
            Observable.error(
                UnknownHostException()
            )
        )

        sut.getUserDetails("unknownUser")

        testObserver.hasSubscription()
        testObserver.assertValues(
            DetailsStatus.Loading(true),
            DetailsStatus.Loading(false),
            DetailsStatus.InternetError
        )
    }

}