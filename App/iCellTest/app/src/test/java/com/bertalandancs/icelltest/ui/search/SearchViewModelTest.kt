package com.bertalandancs.icelltest.ui.search

import com.bertalandancs.icelltest.ViewModelBaseTestClass
import com.bertalandancs.icelltest.data.ServiceErrorException
import com.bertalandancs.icelltest.data.model.Error
import com.bertalandancs.icelltest.data.model.ErrorResponse
import com.bertalandancs.icelltest.data.model.User
import com.bertalandancs.icelltest.data.repositories.SearchRepository
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.kotlin.argThat
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class SearchViewModelTest : ViewModelBaseTestClass() {

    private lateinit var sut: SearchViewModel
    private lateinit var testObserver: TestObserver<SearchStatus>

    @Mock
    lateinit var searchRepository: SearchRepository

    @Before
    fun beforeEach() {
        sut = SearchViewModel(searchRepository)
        testObserver = TestObserver()
    }

    @Test
    fun `when search by query and got ServiceErrorException api limit reached then post ApiLimitReached status`() {
        sut.usersSearchObservable.subscribeWith(testObserver)
        `when`(searchRepository.getUsers(anyString(), anyInt()))
            .thenReturn(
                Observable.error(
                    ServiceErrorException(
                        ErrorResponse(
                            "API rate limit exceeded for XYZ. (But here's the good news: Authenticated requests get a higher rate limit. Check out the documentation for more details.)",
                            ""
                        )
                    )
                )
            )

        sut.getUsers("octo")

        testObserver.hasSubscription()
        testObserver.assertValues(
            SearchStatus.Loading(true),
            SearchStatus.Loading(false),
            SearchStatus.ApiLimitReached
        )
    }

    @Test
    fun `when search by empty query and got ServiceErrorException with missing q field then post MissingQuery status`() {
        sut.usersSearchObservable.subscribeWith(testObserver)
        `when`(searchRepository.getUsers(anyString(), anyInt()))
            .thenReturn(
                Observable.error(
                    ServiceErrorException(
                        ErrorResponse(
                            "Validation Failed",
                            "",
                            ArrayList<Error>().apply {
                                add(
                                    Error(
                                        "",
                                        "Search",
                                        "q",
                                        "missing"
                                    )
                                )
                            }
                        )
                    )
                )
            )

        sut.getUsers("octo")

        testObserver.hasSubscription()
        testObserver.assertValues(
            SearchStatus.Loading(true),
            SearchStatus.Loading(false),
            SearchStatus.MissingQuery
        )
    }

    @Test
    fun `when search by query but got ServiceErrorException with too long query then post QueryInvalid status`() {
        sut.usersSearchObservable.subscribeWith(testObserver)
        `when`(searchRepository.getUsers(anyString(), anyInt()))
            .thenReturn(
                Observable.error(
                    ServiceErrorException(
                        ErrorResponse(
                            "Validation Failed",
                            "",
                            ArrayList<Error>().apply {
                                add(
                                    Error(
                                        "The search is longer than 256 characters.",
                                        "Search",
                                        "q",
                                        "invalid"
                                    )
                                )
                            }
                        )
                    )
                )
            )

        sut.getUsers("octo")

        testObserver.hasSubscription()
        testObserver.assertValues(
            SearchStatus.Loading(true),
            SearchStatus.Loading(false),
            SearchStatus.QueryInvalid
        )
    }

    @Test
    fun `when start a new query but got unknown exception then post UnknownError status`() {
        sut.usersSearchObservable.subscribeWith(testObserver)
        `when`(searchRepository.getUsers(anyString(), anyInt())).thenReturn(
            Observable.error(
                IllegalAccessException()
            )
        )

        sut.getUsers("query")

        testObserver.hasSubscription()
        testObserver.assertValues(
            SearchStatus.Loading(true),
            SearchStatus.Loading(false),
            SearchStatus.UnknownError
        )
    }

    @Test
    fun `when start a new query but got SocketTimeoutException then post InternetError status`() {
        sut.usersSearchObservable.subscribeWith(testObserver)
        `when`(searchRepository.getUsers(anyString(), anyInt())).thenReturn(
            Observable.error(
                SocketTimeoutException()
            )
        )

        sut.getUsers("query")

        testObserver.hasSubscription()
        testObserver.assertValues(
            SearchStatus.Loading(true),
            SearchStatus.Loading(false),
            SearchStatus.InternetError
        )
    }

    @Test
    fun `when start a new query but got UnknownHostException then post InternetError status`() {
        sut.usersSearchObservable.subscribeWith(testObserver)
        `when`(searchRepository.getUsers(anyString(), anyInt())).thenReturn(
            Observable.error(
                UnknownHostException()
            )
        )

        sut.getUsers("query")

        testObserver.hasSubscription()
        testObserver.assertValues(
            SearchStatus.Loading(true),
            SearchStatus.Loading(false),
            SearchStatus.InternetError
        )
    }

    @Test
    fun `when start a new search then post Ok status with new result and new page`() {
        //Simulate 3 search by "octo" query
        val octoUsers = ArrayList<User>().apply {
            add(User("octo1", 1, "avatarUrl", "User"))
            add(User("octo2", 2, "avatarUrl", "User"))
            add(User("octo3", 3, "avatarUrl", "User"))
            add(User("octo4", 4, "avatarUrl", "User"))
        }
        `when`(searchRepository.getUsers(argThat { this == "octo" }, anyInt()))
            .thenReturn(Observable.just(octoUsers))
        sut.getUsers("octo") // -> currentPage 1
        sut.getUsers("octo") // -> currentPage 2
        sut.getUsers("octo") // -> currentPage 3

        //New search by "cat" query which "reset" page counter.
        val catUsers = ArrayList<User>().apply {
            add(User("cat1", 1, "", "User"))
            add(User("cat2", 2, "", "User"))
            add(User("cat3", 3, "", "User"))
            add(User("cat4", 4, "", "User"))
        }
        sut.usersSearchObservable.subscribeWith(testObserver)
        `when`(searchRepository.getUsers("cat"))
            .thenReturn(
                Observable.just(catUsers)
            )

        sut.getUsers("cat")

        testObserver.hasSubscription()
        testObserver.assertValues(
            SearchStatus.Loading(true),
            SearchStatus.Loading(false),
            SearchStatus.Ok(catUsers, 1)
        )
    }

    @Test
    fun `when search by query and no errors then post Ok status with result and currentPage`() {
        val usersList = ArrayList<User>().apply {
            add(User("login1", 1, "avatarUrl", "User"))
            add(User("login2", 2, "avatarUrl", "User"))
            add(User("login3", 3, "avatarUrl", "User"))
            add(User("login4", 4, "avatarUrl", "User"))
        }
        sut.usersSearchObservable.subscribeWith(testObserver)
        `when`(searchRepository.getUsers("octo"))
            .thenReturn(
                Observable.just(usersList)
            )

        sut.getUsers("octo")

        testObserver.hasSubscription()
        testObserver.assertValues(
            SearchStatus.Loading(true),
            SearchStatus.Loading(false),
            SearchStatus.Ok(usersList, 1)
        )
    }
}