package com.bertalandancs.icelltest.data.repositories

import com.bertalandancs.icelltest.data.api.GithubService
import com.bertalandancs.icelltest.data.model.SearchUserResponse
import com.bertalandancs.icelltest.data.model.User
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SearchRepositoryImplTest {

    private lateinit var sut: SearchRepository
    private lateinit var testObserver: TestObserver<List<User>>

    @Mock
    lateinit var githubService: GithubService

    @Before
    fun beforeEach() {
        testObserver = TestObserver()
        sut = SearchRepositoryImpl(githubService)
    }

    @Test
    fun `When getUsers response has list by query then return a list with these users`() {
        val expectedUsers = ArrayList<User>().apply {
            add(
                User(
                    "login",
                    1,
                    "https://m.media-amazon.com/images/M/MV5BMTYwOTEwNjAzMl5BMl5BanBnXkFtZTcwODc5MTUwMw@@._V1_FMjpg_UX1000_.jpg",
                    "User"
                )
            )
            add(
                User(
                    "login2",
                    2,
                    "https://m.media-amazon.com/images/M/MV5BMTYwOTEwNjAzMl5BMl5BanBnXkFtZTcwODc5MTUwMw@@._V1_FMjpg_UX1000_.jpg",
                    "User"
                )
            )
            add(
                User(
                    "login3",
                    3,
                    "https://m.media-amazon.com/images/M/MV5BMTYwOTEwNjAzMl5BMl5BanBnXkFtZTcwODc5MTUwMw@@._V1_FMjpg_UX1000_.jpg",
                    "User"
                )
            )
            add(
                User(
                    "login4",
                    4,
                    "https://m.media-amazon.com/images/M/MV5BMTYwOTEwNjAzMl5BMl5BanBnXkFtZTcwODc5MTUwMw@@._V1_FMjpg_UX1000_.jpg",
                    "User"
                )
            )
        }
        `when`(githubService.getUsers("User")).thenReturn(
            Observable.just(
                SearchUserResponse(40, false, expectedUsers)
            )
        )

        sut.getUsers("User").subscribeWith(testObserver)

        testObserver.hasSubscription()
        testObserver.assertValue {
            it.size == 4 && it == expectedUsers
        }
    }

}